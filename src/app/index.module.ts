/// <reference path="../../.tmp/typings/tsd.d.ts" />

import { config } from './index.config';
import { routes } from './index.routes';
import { run } from './index.run';

// Constants
import { BackEndUrl } from './components/core/backEnd/backEndUrl.constant';
import { Routes } from './components/core/routes/routes.constant';

// Services
import { BackEnd } from './components/core/backEnd/backEnd.service';
import { Configurator } from './components/core/configurator/configurator.service';
import { Guardian } from './components/core/guardian/guardian.service';
import { Translator } from './components/core/translations/translator.service';

// Directives
import { dropDown } from './components/shell/ui/dropDown/dropDown.directive';
import { languagesBar } from './components/shell/ui/languagesBar/languagesBar.directive';
import { shell } from './components/shell/shell.directive';
import { topBar } from './components/shell/ui/topBar/topBar.directive';
import { translate } from './components/core/translations/translate.directive';
import { uiLocker } from './components/shell/ui/uiLocker/uiLocker.directive';

// Controllers
import { ConnectionController } from './connection/connection.controller';
import { DashboardsController } from './dashboards/dashboards.controller';
import { OrdersController } from './orders/orders.controller';
import { ProductsController } from './products/products.controller';
import { ReportsController } from './reports/reports.controller';
import { SettingsController } from './settings/settings.controller';
import { SitesController } from './settings/sites/sites.controller';
import { UsersController } from './users/users.controller';

// Facades
import { ShellFacade } from './components/shell/shell.facade';
import { ConnectionFacade } from './connection/connection.facade';
import { ProductsFacade } from './products/products.facade';
import { UsersFacade } from './users/users.facade.mock'; /* mock */

// Filters
import { usersFilter } from './users/users.filter';

module presto {
  'use strict';

  angular.module('presto', ['ngMessages', 'ngAria', 'ui.router', 'ui.bootstrap', 'webStorageModule', 'angularRipple'])
    .config(config)
    .config(routes)
    .run(run)
    // Constants
    .constant('backEndUrl', BackEndUrl.get())
    .constant('environment', 'ACC') /* env */
    .constant('routes', Routes.get())
    .constant('useDataMockUps', true)
    .constant('defaultLanguage', 'EN')
    // Providers
    // Services
    .service('backEnd', BackEnd)
    .service('guardian', Guardian)
    .service('configurator', Configurator)
    .service('translator', Translator)
    // Directives
    .directive('dropDown', dropDown)
    .directive('languagesBar', languagesBar)
    .directive('shell', shell)
    .directive('topBar', topBar)
    .directive('translate', translate)
    .directive('uiLocker', uiLocker)
    // Controllers
    .controller('ConnectionController', ConnectionController)
    .controller('DashboardsController', DashboardsController)
    .controller('OrdersController', OrdersController)
    .controller('ProductsController', ProductsController)
    .controller('ReportsController', ReportsController)
    .controller('SettingsController', SettingsController)
    .controller('SitesController', SitesController)
    .controller('UsersController', UsersController)
    // Facades
    .service('shellFacade', ShellFacade)
    .service('connectionFacade', ConnectionFacade)
    .service('productsFacade', ProductsFacade)
    .service('usersFacade', UsersFacade)
    // Filters
    .filter('usersFilter', usersFilter)
    ;
}
