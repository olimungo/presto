import { IRoute } from './components/core/routes/routes.interface';
import { IGuardian } from './components/core/guardian/guardian.interface';

/* @ngInject */
export function routes($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider, routes: IRoute[]) {
  routes.map((route: IRoute) => {
    $stateProvider
      .state(route.state, {
        url: route.url,
        templateUrl: route.template,
        controller: route.controller,
        controllerAs: 'vm',
        resolve: {
          /* @ngInject */
          isRouteAuthorised: (guardian: IGuardian) => {
            return guardian.isRouteAuthorised(route.roles);
          }
        }
      });
  });

  // Setting the default route
  var defaultRoutes = routes.filter((route: IRoute) => route.defaultRoute);

  if (defaultRoutes.length > 0) {
    $urlRouterProvider.otherwise(defaultRoutes[0].url);
  }
}
