import { IIndexController } from './index.interface';
import { ITranslator } from './components/translations/translator.interface';
import { Events, EventsHelper } from './components/events/events.helper';

export class IndexController implements IIndexController {
  translationsLoaded: boolean;

  /* @ngInject */
  constructor (private $rootScope: ng.IRootScopeService, private $timeout: ng.ITimeoutService, private translator: ITranslator) {
    this.setListeners();
    this.loadTranslations();
  }

  private setListeners() {
    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UserChangedLanguage), () => {
      this.loadTranslations();
    });
  }

  private loadTranslations() {
    this.translationsLoaded = false;

    this.translator.loadTranslations().then(() => {
      this.$timeout(() => {
        this.translationsLoaded = true;

        this.$timeout(() => {
          this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UiRefresh));
        }, 0);
      }, 0);
    });
  }
}
