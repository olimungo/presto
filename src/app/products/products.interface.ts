export interface IProductsFacade {
  getProducts(): ng.IPromise<any>;
}

export interface IProductsController {
}