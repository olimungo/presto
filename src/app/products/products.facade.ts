import { IProductsFacade } from './products.interface';
import { IProduct, Product } from '../components/products/product.entity';
import { IBackEnd } from '../components/core/backEnd/backEnd.interface';

export class ProductsFacade implements IProductsFacade {
  /** @ngInject */
  constructor(private backEnd: IBackEnd) {
  }

  getProducts(): ng.IPromise<any> {
    return this.backEnd.call({method: 'GET', url: 'products', responseClass: Product}).then((data: IProduct[]) => {
      return data;
    });
  }
}
