import { IProductsController, IProductsFacade } from './products.interface';
import { IProduct } from '../components/products/product.entity';

export class ProductsController implements IProductsController {
  products: IProduct[] = [];

  /* @ngInject */
  constructor (private productsFacade: IProductsFacade) {
    this.productsFacade.getProducts().then((products: IProduct[]) => {
      this.products = products;
      console.log(this.products);
    });
  }
}
