import { IProduct, Product } from '../components/products/product.entity';
import { IBackEnd } from '../components/backEnd/backEnd.interface';

interface IProductsService {
  getProducts(): void;
}

export class ProductsService implements IProductsService {
  /** @ngInject */
  constructor(private backEnd: IBackEnd) {
  }

  getProducts(): void {
    this.backEnd.call({method: 'GET', url: 'products', responseClass: Product}).then((data: IProduct[]) => {
      // console.log('Products: ', data);
    });
  }
}
