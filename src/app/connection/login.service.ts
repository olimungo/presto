import { Guardian } from '../components/guardian/guardian.service';

export interface ILoginService {
  login(username: string): void;
}

export class LoginService implements ILoginService {
  /** @ngInject */
  constructor(private guardian: Guardian) {
  }

  login(username: string): void {
    this.guardian.login(username);
  }

  logout(): void {
    this.guardian.logout();
  }
}
