import { LoginService } from './login.service';

export class LogoutController {
  /* @ngInject */
  constructor (loginService: LoginService, private $location: ng.ILocationService) {
    loginService.logout();
  }

  logout(): void {
    this.$location.path('/login');
  }
}
