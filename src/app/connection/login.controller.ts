import { LoginService } from './login.service';

export class LoginController {
  username = 'oli';
  password = 'toto';

  /* @ngInject */
  constructor (private loginService: LoginService) {
  }

  login() {
    this.loginService.login(this.username);
  }
}
