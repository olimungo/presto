export interface IConnectionController {
  login(): void;
  goToLogIn(): void;
}

export interface IConnectionFacade {
  login(username: string): void;
  goToLogIn(): void
}