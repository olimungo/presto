import { EEvents, EEventsHelper } from '../components/shell/events/events.helper';
import { IConnectionFacade } from './connection.interface';
import { IRoute, ERoutes } from '../components/core/routes/routes.interface';

export class ConnectionFacade implements IConnectionFacade {
  /* @ngInject */
  constructor(private $rootScope: ng.IRootScopeService, private $location: ng.ILocationService, private routes: IRoute[]) {
  }

  login(username: string): void {
    this.$rootScope.$emit(EEventsHelper.enumToAcronym(EEvents.UserLoggedIn), username);
  }

  goToLogIn(): void {
    var loginRoutes = this.routes.filter((route: IRoute) => route.id === ERoutes.Login);

    if (loginRoutes.length > 0) {
      this.$location.path('/login');
    }
  }
}
