import { IConnectionController, IConnectionFacade } from './connection.interface';

export class ConnectionController implements IConnectionController {
  username = 'oli';
  password = 'toto';

  /* @ngInject */
  constructor (private connectionFacade: IConnectionFacade) {
  }

  login(): void {
    this.connectionFacade.login(this.username);
  }

  goToLogIn(): void {
    this.connectionFacade.goToLogIn();
  }
}
