import { IAcronym } from '../../core/acronyms/acronyms.interface';
import { AcronymsHelper } from '../../core/acronyms/acronyms.helper';

export enum EEvents { UserLoggedIn, SiteChanged, LockUi }

export class EEventsHelper {
  static events: IAcronym[] = [
    { id: EEvents.UserLoggedIn, value: 'user.loggedIn' },
    { id: EEvents.SiteChanged, value: 'site.changed' },
    { id: EEvents.LockUi, value: 'lock.ui' }
  ];

  static enumToAcronym(event: EEvents): string {
    return AcronymsHelper.enumToAcronym(this.events, event);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.events, acronym);
  }
}
