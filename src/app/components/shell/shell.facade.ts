import { IShellFacade } from './shell.interface';
import { ETranslateKeys } from '../core/translations/translateKeys.helper';
import { IGuardian } from '../core/guardian/guardian.interface';
import { ITranslator } from '../core/translations/translator.interface';
import { IConfigurator } from '../core/configurator/configurator.interface';
import { EConfigs } from '../core/configurator/configs.helper';
import { ELanguages, ELanguagesHelper } from '../core/translations/languages.helper';
import { ERoles, ERolesHelper } from '../core/users/roles.helper';
import { IUser } from '../core/users/user.interface';
import { IDropDownItem } from './ui/dropDown/dropDown.interface';
import { IRoute, EMenuTypes } from '../core/routes/routes.interface';
import { ESites, ESitesHelper } from '../core/users/sites.helper';

export class ShellFacade implements IShellFacade {
  /* @ngInject */
  constructor(private $rootScope: ng.IRootScopeService, private $location: ng.ILocationService,
    private translator: ITranslator, private configurator: IConfigurator,
    private guardian: IGuardian, private routes: IRoute[]) {
    this.setListeners();
  }

  login(username: string): ng.IPromise<any> {
    return this.guardian.login(username).then((user: IUser) => {
      var defaultRoleAcronym = user.rolesAcronym[0];

      user.role = ERolesHelper.acronymToEnum(this.configurator.get(EConfigs.UserCurrentRole, defaultRoleAcronym));

      // Check if role in config is still in the user's roles
      // If not, override with the first from the ones just retrieved
      if (user.roles.indexOf(user.role) < 0) {
        user.role = ERolesHelper.acronymToEnum(defaultRoleAcronym);
        this.configurator.set(EConfigs.UserCurrentRole, defaultRoleAcronym);
      }

      this.configurator.set(EConfigs.UserCurrentRole, ERolesHelper.enumToAcronym(user.role));

      // Navigate to default route if specified in defaultForRoles property
      this.defaultRoute(user.role);

      return user;
    });
  }

  logout(): ng.IPromise<any> {
    return this.guardian.logout();
  }

  defaultRoute(role: ERoles): void {
    var defaultRoutes = this.routes.filter((route: IRoute) => this.guardian.isRoleIn(role, route.defaultForRoles));

    if (defaultRoutes.length > 0) {
      this.$location.path(defaultRoutes[0].url);
    }
  }

  getSite(): string {
    var site = this.configurator.get(EConfigs.UserSite);
    site = site ? site : '';

    return this.translator.translateCode(ETranslateKeys.Sites, site.toLowerCase());
  }

  setSite(site: ESites): void {
    this.configurator.set(EConfigs.UserSite, ESitesHelper.enumToAcronym(site));
  }

  getRole(role: ERoles): string {
    return this.translator.translateCode(ETranslateKeys.Roles, ERolesHelper.enumToAcronym(role).toLowerCase());
  }

  getLanguage(): ELanguages {
    return this.translator.currentLanguage;
  }

  loadTranslations(language?: ELanguages): ng.IPromise<any> {
    if (language !== undefined) {
      this.configurator.set(EConfigs.UserLanguage, ELanguagesHelper.enumToAcronym(language));
    }

    return this.translator.loadTranslations();
  }

  getItemsForRolesMenu(roles: ERoles[]): IDropDownItem[] {
    return roles.map((role: ERoles) => {
      return {
        id: role,
        label: this.translator.translateCode(ETranslateKeys.Roles, ERolesHelper.enumToAcronym(role).toLowerCase()),
        originalObject: role
      };
    });
  }

  getItemsForMenu(menuType: EMenuTypes, role: ERoles): IDropDownItem[] {
    var filteredRoutesByRoles = this.routes.filter((route: IRoute) => this.guardian.isRoleIn(role, route.roles));

    var menu: IRoute[] = filteredRoutesByRoles
      .filter((route: IRoute) => route.menuType === menuType)
      .sort(this.sortRoutes);

    return menu.map((route: IRoute) => {
      return {
        id: route.id,
        label: this.translator.translateCode(ETranslateKeys.Menus, route.state.toLowerCase()),
        icon: route.icon,
        borderTop: route.borderTop,
        originalObject: route
      };
    });
  }

  changeMenu(route: IRoute): void {
    this.$location.path(route.url);
  }

  changeRole(role: ERoles): void {
    this.configurator.set(EConfigs.UserCurrentRole, ERolesHelper.enumToAcronym(role));
    this.defaultRoute(role);
    this.guardian.changeRole(role);
  }

  private setListeners(): void {
    this.$rootScope.$on('$stateChangeError', (event: any) => {
      event.preventDefault();

      if (this.guardian.isUserAuthenticated()) {
        this.defaultRoute(ERolesHelper.acronymToEnum(this.configurator.get(EConfigs.UserCurrentRole)));
      } else {
        var defaultRoutes = this.routes.filter((route: IRoute) => route.defaultRoute);

        if (defaultRoutes.length > 0) {
          this.$location.path(defaultRoutes[0].url);
        }
      }
    });
  }

  private sortRoutes(route1: IRoute, route2: IRoute): number {
    return route1.order - route2.order;
  }
}
