import { IIndexController } from './index.interface';
import { ITranslator } from './components/translations/translator.interface';
import { Events, EventsHelper } from './components/shell/events/events.helper';
import { IIndexFacade } from './index.interface';
import { IUser } from './components/users/user.entity';
import { Roles } from './components/users/roles.helper';
import { IDropDownItem } from './components/shell/dropDown/dropDown.interface';
import { Languages } from './components/users/languages.helper';
import { IMenuItems } from './components/shell/topBar/topBar.interface';
import { TranslateKeys } from './components/translations/translateKeys.helper';
import { IAuthorisedRoutes } from './components/routesManager/routesManager.interface';
import { IRoute } from './components/routesManager/routesManager.interface';

export class ShellController implements IShellController {
  translationsLoaded: boolean;

  userFullName: string = null;
  userDgUnit: string;
  language: Languages;
  site: string;
  environment: string;
  rolesItems: IDropDownItem[];
  userRole: string;
  generalMenuItems : IMenuItems[] = [];
  settingsMenuItems : IDropDownItem[] = [];

  /* @ngInject */
  constructor (private $rootScope: ng.IRootScopeService, private $timeout: ng.ITimeoutService,
    private $location: ng.ILocationService, private translator: ITranslator, private indexFacade: IIndexFacade) {
    this.setListeners();
    this.loadTranslations();
  }

  changeMenu(route: IRoute): void {
    this.$location.path(route.url);
  }

  changeRole(role: Roles): void {
    this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChangedRole), role);
  }

  changeLanguage(language: Languages): void {
    this.loadTranslations();

    // TODO: save language in local storage
  }

  private setListeners(): void {
    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UserChanged), (event: any, user: IUser) => {
      if (user) {
        this.userFullName = user.fullName;
        this.userDgUnit = user.dgUnit;
        this.language = user.currentLanguage;
        this.rolesItems = this.indexFacade.getItemsForRolesMenu(user.roles);
        this.userRole = this.indexFacade.getRole(user.currentRole);
      } else {
        this.userFullName = null;
        this.userDgUnit = null;
        this.language = null;
        this.rolesItems = [];
        this.userRole = null;
      }
    });

    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.RoutesAuthorisedChanged), (event: any, routes: IAuthorisedRoutes) => {
      this.generalMenuItems = this.indexFacade.getItemsForGeneralMenu(routes.generalMenuItems);
      this.settingsMenuItems = this.indexFacade.getItemsForSettingsMenu(routes.settingsMenuItems);
    });

    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.SiteChanged), (event: any, site: string) => {
      this.site = this.translator.translateCode(TranslateKeys.Sites, site.toLowerCase());
    });
  }

  private loadTranslations() {
    this.translationsLoaded = false;

    this.translator.loadTranslations().then(() => {
      this.$timeout(() => {
        this.translationsLoaded = true;
        this.site = this.indexFacade.getSite();
        this.language = this.translator.currentLanguage;

        this.$timeout(() => {
          this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UiRefresh));
        }, 0);
      }, 0);
    });
  }
}
