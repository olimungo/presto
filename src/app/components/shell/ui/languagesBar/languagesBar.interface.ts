import { ELanguages } from '../../../core/translations/languages.helper';

export interface ILanguagesBarController {
  languageEn: ELanguages;
  languageFr: ELanguages;
  language: ELanguages;
  changeLanguage(language: ELanguages): void;
}