import { ELanguages } from '../../../core/translations/languages.helper';
import { ILanguagesBarController } from './languagesBar.interface';

export function languagesBar(): ng.IDirective {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'app/components/shell/ui/languagesBar/languagesBar.html',
    controller: LanguagesBarController,
    controllerAs: 'vm',
    bindToController: {
      language: '=',
      onChangeLanguage: '&'
    }
  };
}

class LanguagesBarController implements ILanguagesBarController {
  languageEn: ELanguages = ELanguages.English;
  languageFr: ELanguages = ELanguages.French;
  language: ELanguages;
  onChangeLanguage: Function;

  changeLanguage(language: ELanguages): void {
    this.onChangeLanguage({ language: language });
  }
}
