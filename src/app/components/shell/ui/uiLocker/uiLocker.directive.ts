import { IUiLockerController } from './uiLocker.interface';
import { EEvents, EEventsHelper } from '../../events/events.helper';

export function uiLocker(): ng.IDirective {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'app/components/shell/ui/uiLocker/uiLocker.html',
    controller: UiLockerController,
    controllerAs: 'vm',
    bindToController: true
  };
}

class UiLockerController implements IUiLockerController {
  isVisible: boolean = false;

  /* @ngInject */
  constructor(private $rootScope: ng.IRootScopeService, private $timeout: ng.ITimeoutService) {
    this.setListeners();
  }

  private setListeners(): void {
    this.$rootScope.$on(EEventsHelper.enumToAcronym(EEvents.LockUi), (event: any, promise: ng.IPromise<any>) => {
      var start: number = new Date().getTime();

      var delay: ng.IPromise<void> = this.$timeout(() => {
        this.isVisible = true;
        console.log('delay');
      }, 500);

      promise.then(() => {
        var end: number = new Date().getTime();

        this.$timeout.cancel(delay);

        // Prevent flickering by displaying the timer at least 2 second
        this.$timeout(() => {
          this.isVisible = false;
        }, start - end + 2500);
      });
    });
  }
}
