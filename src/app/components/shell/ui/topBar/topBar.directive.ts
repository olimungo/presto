import { IRoute } from '../../../core/routes/routes.interface';
import { ERoles } from '../../../core/users/roles.helper';
import { ITopBarController } from './topBar.interface';
import { ELanguages } from '../../../core/translations/languages.helper';
import { ESites } from '../../../core/users/sites.helper';

export function topBar(): ng.IDirective {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'app/components/shell/ui/topBar/topBar.html',
    controller: TopBarController,
    controllerAs: 'vm',
    bindToController: {
      userFullName: '=',
      userDgUnit: '=',
      userRole: '=',
      language: '=',
      site: '=',
      generalMenuItems: '=',
      settingsMenuItems: '=',
      rolesItems: '=',
      onChangeRole: '&',
      onChangeMenu: '&',
      onChangeLanguage: '&',
      onChangeSite: '&'
    }
  };
}

class TopBarController implements ITopBarController {
  env: string;
  onChangeRole: Function;
  onChangeMenu: Function;
  onChangeLanguage: Function;
  onChangeSite: Function;

  /* @ngInject */
  constructor(private environment: string) {
    this.env = environment;
  }

  changeMenu(route: IRoute): void {
    this.onChangeMenu({ route: route });
  }

  changeRole(role: ERoles): void {
    this.onChangeRole({ role: role });
  }

  changeLanguage(language: ELanguages): void {
    this.onChangeLanguage({ language: language });
  }

  changeSite(site: ESites): void {
    this.onChangeLanguage({ site: site });
  }
}
