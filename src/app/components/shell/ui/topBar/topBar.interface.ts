import { IRoute } from '../../../core/routes/routes.interface';
import { ERoles } from '../../../core/users/roles.helper';
import { ELanguages } from '../../../core/translations/languages.helper';
import { ESites } from '../../../core/users/sites.helper';

export interface ITopBarController {
  env: string;
  changeMenu(item: IRoute): void;
  changeRole(item: ERoles): void;
  changeLanguage(language: ELanguages): void;
  changeSite(site: ESites): void;
}

export interface IMenuItems {
  id: any;
  label: string;
  icon?: string; // Icon in the menu
  originalObject?: any;
}