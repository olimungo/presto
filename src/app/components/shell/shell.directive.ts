import { IShellController, IShellFacade  } from './shell.interface';
import { IDropDownItem } from './ui/dropDown/dropDown.interface';
import { EEvents, EEventsHelper } from './events/events.helper';
import { ELanguages } from '../core/translations/languages.helper';
import { ERoles } from '../core/users/roles.helper';
import { ESites } from '../core/users/sites.helper';
import { IUser } from '../core/users/user.interface';
import { IMenuItems } from './ui/topBar/topBar.interface';
import { IRoute, EMenuTypes, ERoutes } from '../core/routes/routes.interface';

export function shell(): ng.IDirective {
  return {
    restrict: 'E',
    bindToController: {
    },
    templateUrl: 'app/components/shell/shell.html',
    controller: ShellController,
    controllerAs: 'vm',
    scope: {},
    transclude: true
  };
}

class ShellController implements IShellController {
  translationsLoaded: boolean;

  userFullName: string = null;
  userDgUnit: string;
  language: ELanguages;
  site: string;
  environment: string;
  rolesItems: IDropDownItem[];
  userRole: string;
  generalMenuItems : IMenuItems[] = [];
  settingsMenuItems : IDropDownItem[] = [];

  private role: ERoles;
  private roles: ERoles[];

  /* @ngInject */
  constructor (private $rootScope: ng.IRootScopeService, private shellFacade: IShellFacade,
    private $document: ng.IDocumentService, private $timeout: ng.ITimeoutService) {
    this.setListeners();

    // Delaying to be sure that all components are ready
    $document.ready(() => {
      this.$timeout(() => {
        this.loadTranslations();
      }, 100);
    });
  }

  changeMenu(route: IRoute): void {
    if (route.id === ERoutes.Logout) {
      this.logout();
    }

    this.shellFacade.changeMenu(route);
  }

  changeRole(role: ERoles): void {
    this.shellFacade.changeRole(role);

    this.userRole = this.shellFacade.getRole(role);
    this.role = role;
    this.generalMenuItems = this.shellFacade.getItemsForMenu(EMenuTypes.General, role);
    this.settingsMenuItems = this.shellFacade.getItemsForMenu(EMenuTypes.Settings, role);
  }

  changeLanguage(language: ELanguages): void {
    this.loadTranslations(language);
  }

  private setListeners(): void {
    this.$rootScope.$on(EEventsHelper.enumToAcronym(EEvents.UserLoggedIn), (event: any, username: string) => {
      this.shellFacade.login(username).then((user: IUser) => {
        this.userFullName = user.fullName;
        this.userDgUnit = user.dgUnit;
        this.roles = user.roles;
        this.rolesItems = this.shellFacade.getItemsForRolesMenu(user.roles);
        this.userRole = this.shellFacade.getRole(user.role);
        this.role = user.role;
        this.generalMenuItems = this.shellFacade.getItemsForMenu(EMenuTypes.General, user.role);
        this.settingsMenuItems = this.shellFacade.getItemsForMenu(EMenuTypes.Settings, user.role);
      });
    });

    this.$rootScope.$on(EEventsHelper.enumToAcronym(EEvents.SiteChanged), (event: any, site: ESites) => {
      this.shellFacade.setSite(site);
      this.site = this.shellFacade.getSite();
    });
  }

  private loadTranslations(language?: ELanguages): void {
    var translationsLoaded = this.shellFacade.loadTranslations(language);

    this.translationsLoaded = false;

    this.$rootScope.$emit(EEventsHelper.enumToAcronym(EEvents.LockUi), translationsLoaded);

    translationsLoaded.then(() => {
      this.translationsLoaded = true;
      this.site = this.shellFacade.getSite();
      this.language = this.shellFacade.getLanguage();

      if (this.roles &&  this.role) {
        this.rolesItems = this.shellFacade.getItemsForRolesMenu(this.roles);
        this.userRole = this.shellFacade.getRole(this.role);
        this.generalMenuItems = this.shellFacade.getItemsForMenu(EMenuTypes.General, this.role);
        this.settingsMenuItems = this.shellFacade.getItemsForMenu(EMenuTypes.Settings, this.role);
      }
    });
  }

  private logout(): void {
    this.shellFacade.logout().then(() => {
      this.userFullName = null;
      this.userDgUnit = null;
      this.rolesItems = [];
      this.roles = null;
      this.userRole = null;
      this.role = null;
      this.generalMenuItems = [];
      this.settingsMenuItems = [];
    });
  }
}
