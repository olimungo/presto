import { IDropDownItem } from './ui/dropDown/dropDown.interface';
import { ERoles } from '../core/users/roles.helper';
import { ELanguages } from '../core/translations/languages.helper';
import { ESites } from '../core/users/sites.helper';
import { IMenuItems } from './ui/topBar/topBar.interface';
import { IRoute, EMenuTypes } from '../core/routes/routes.interface';

export interface IShellFacade {
  login(username: string): ng.IPromise<any>;
  logout(): ng.IPromise<any>;
  defaultRoute(role: ERoles): void;
  getSite(): string;
  setSite(site: ESites): void;
  getRole(roles: ERoles): string;
  getLanguage(): ELanguages;
  loadTranslations(language?: ELanguages): ng.IPromise<any>;
  getItemsForRolesMenu(roles: ERoles[]): IDropDownItem[];
  getItemsForMenu(menuType: EMenuTypes, roles: ERoles): IDropDownItem[];
  changeMenu(route: IRoute): void;
  changeRole(role: ERoles): void;
}

export interface IShellController {
  // To allows to hide the view's content while the translations are loaded.
  translationsLoaded: boolean;

  // The user's full name
  userFullName: string;

  // The user's DG and unit
  userDgUnit: string;

  // The language in which the site is set
  language: ELanguages;

  // To display the site (Brussels, Ispra, College, CIE) in the topBar
  site: string;

  // To display the environment (DEV, TEST, ACC or nothing for PROD)
  environment: string;

  // The current user's role
  userRole: string;

  // Items in the horizontal menu
  generalMenuItems: IMenuItems[];

  // Items in the settings menu
  settingsMenuItems: IDropDownItem[];

  // When the user selects a item in the horizontal menu or in the settings menu
  changeMenu(route: IRoute): void;

  // When the user changes its role
  changeRole(role: ERoles): void;

  // When the user changes the site's language
  changeLanguage(language: ELanguages): void;
}