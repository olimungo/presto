import { Routes } from './routes.helper';
import { Roles } from '../users/roles.helper';
import { MenuTypes } from '../ui/topBar/menuTypes.helper';

export interface IRoutesManagerProvider {
  routes: IRoute[];
  defaultRoute: string;
}

export interface IAuthorisedRoutes {
  generalMenuItems: IRoute[];
  settingsMenuItems: IRoute[];
  defaultRoute: IRoute;
}

export interface IRoute {
  id: Routes;
  label: string;
  url: string;
  state: string;
  template: string;
  controller: string;
  roles: Roles[];
  menuType?: MenuTypes; // In which menu the route has to be displayed
  order?: number; // The order in which the route has to be displayed
  icon?: string; // Icon in the menu
  borderTop?: boolean; // If a top border has to be drawn in the Settings menu
  defaultForRoles?: Roles[];
}