import { Roles } from '../users/roles.helper';
import { IUser } from '../users/user.entity';
import { Routes } from './routes.helper';
import { IRoutesManagerProvider, IAuthorisedRoutes, IRoute } from './routesManager.interface';
import { MenuTypes } from '../ui/topBar/menuTypes.helper';
import { Events, EventsHelper } from '../events/events.helper';

export class RoutesManager implements ng.IServiceProvider, IRoutesManagerProvider {
  private _routes: IRoute[] = [
    {
      id: Routes.Dashboards, label: 'Dashboards', url: '/dashboards',  state: 'dashboards', template: 'app/dashboards/dashboards.html',
      controller: 'DashboardsController', roles: [ Roles.Requestor, Roles.OrderAdministrator, Roles.Administrator ],
      menuType: MenuTypes.General, order: 1, icon: 'fa-bar-chart',
      defaultForRoles : [ Roles.OrderAdministrator ]
    },
    {
      id: Routes.Login, label: 'Log in', url: '/login',  state: 'login', template: 'app/connection/login.html',
      controller: 'LoginController', roles: [ Roles.Anonymous ]
    },
    {
      id: Routes.LogOut, label: 'Log out', url: '/logout',  state: 'logout', template: 'app/connection/logout.html',
      controller: 'LogoutController', roles: [ Roles.All ], menuType: MenuTypes.Settings, order: 4,
      icon: 'fa-sign-out', borderTop: true
    },
    {
      id: Routes.Orders, label: 'Orders', url: '/orders',  state: 'orders', template: 'app/orders/orders.html',
      controller: 'OrdersController',
      roles: [
        Roles.Requestor, Roles.CounterSigning, Roles.PrestataireDrinks, Roles.PrestataireBanqueting, Roles.PrestataireConference,
        Roles.PrestatairePrivate, Roles.Administrator
      ],
      menuType: MenuTypes.General, order: 2, icon: 'fa-file-text-o',
      defaultForRoles : [ Roles.Requestor ]
    },
    {
      id: Routes.Catalog, label: 'Products', url: '/products',  state: 'products', template: 'app/products/products.html',
      controller: 'ProductsController', roles: [ Roles.Administrator ], menuType: MenuTypes.Settings, order: 1, icon: 'fa-sitemap',
      defaultForRoles: [ Roles.Administrator ]
    },
    {
      id: Routes.Reports, label: 'Reports', url: '/reports',  state: 'reports', template: 'app/reports/reports.html',
      controller: 'ReportsController',
      roles: [
        Roles.Requestor, Roles.OrderAdministrator, Roles.CounterSigning, Roles.PrestataireDrinks, Roles.PrestataireBanqueting,
        Roles.PrestataireConference, Roles.PrestatairePrivate, Roles.Administrator
      ],
      menuType: MenuTypes.General, order: 3, icon: 'fa-chart'
    },
    {
      id: Routes.Settings, label: 'Settings', url: '/settings',  state: 'settings', template: 'app/settings/settings.html',
      controller: 'SettingsController', roles: [ Roles.Administrator ], menuType: MenuTypes.Settings, order: 3, icon: 'fa-cog'
    },
    {
      id: Routes.Settings, label: 'Sites', url: '/settings/sites',  state: 'settings/sites', template: 'app/settings/sites/sites.html',
      controller: 'SitesController', roles: [ Roles.Administrator ]
    },
    {
      id: Routes.Users, label: 'Users', url: '/users',  state: 'users', template: 'app/users/users.html',
      controller: 'UsersController', roles: [ Roles.Administrator ], menuType: MenuTypes.Settings, order: 2, icon: 'fa-users'
    },
  ];

  private _defaultRoute = this.getRouteById(Routes.Login).url;

  private $rootScope;
  private $location;

  get routes(): IRoute[] {
    return angular.copy(this._routes);
  }

  get defaultRoute(): string {
    return this._defaultRoute;
  }

  /** @ngInject */
  $get($rootScope: ng.IRootScopeService, $location: ng.ILocationService): void {
    this.$rootScope = $rootScope;
    this.$location = $location;
    this.setListeners();
  }

  private setListeners(): void {
    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UserChanged), (event: any, user: IUser) => {
      if (user) {
        this.setAllowedRoutes(user.currentRole);
      } else {
        this.setAllowedRoutes(Roles.Anonymous);
      }
    });
  }

  private setAllowedRoutes(currentRole?: Roles) {
    var filteredRoutes = this.filterRoutes(currentRole);

    this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.RoutesAuthorisedChanged), filteredRoutes);

    if (filteredRoutes.defaultRoute !== null) {
      this.$location.path(filteredRoutes.defaultRoute.url);
    }
  }

  private filterRoutes(currentRole: Roles): IAuthorisedRoutes {
    var filteredRoutesByRoles = this._routes
      .filter((route: IRoute) => this.isRoleIn(currentRole, route.roles));

    var defaultRoutes = filteredRoutesByRoles.filter((route: IRoute) => {
      return this.isRoleIn(currentRole, route.defaultForRoles);
    });

    var defaultRoute = null;

    if (defaultRoutes.length > 0) {
      defaultRoute = defaultRoutes[0];
    }

    var generalMenuItems: IRoute[] = filteredRoutesByRoles
      .filter((route: IRoute) => route.menuType === MenuTypes.General)
      .sort(this.sortRoutes);

    var settingsMenuItems: IRoute[] = filteredRoutesByRoles
      .filter((route: IRoute) => route.menuType === MenuTypes.Settings)
      .sort(this.sortRoutes);

    return {
      generalMenuItems: generalMenuItems,
      settingsMenuItems: settingsMenuItems,
      defaultRoute: defaultRoute
    };
  }

  private sortRoutes(route1: IRoute, route2: IRoute) {
    return route1.order - route2.order;
  }

  private isRoleIn(currentRole: Roles, rolesRoute: Roles[]): boolean {
    rolesRoute = rolesRoute || [];

    return rolesRoute.some((role: Roles) => ((role === Roles.All && currentRole !== Roles.Anonymous) || role === currentRole));
  }

  private getRouteById(id: Routes) {
    var result = this._routes.filter((route: IRoute) => route.id === id);

    return result.length > 0 ? result[0] : null;
  }
}
