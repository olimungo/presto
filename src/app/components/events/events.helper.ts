import { IAcronym, AcronymsHelper } from '../acronyms/acronyms.helper';

export enum Events {
  UserChangedRole, UserChanged, UserChangedLanguage,
  RoutesAuthorisedChanged, SiteChanged, UiRefresh }

export class EventsHelper {
  static events: IAcronym[] = [
    { id: Events.UserChanged, value: 'user.userChanged' },
    { id: Events.UserChangedRole, value: 'user.changedRole.' },
    { id: Events.UserChangedLanguage, value: 'user.changedLanguage' },
    { id: Events.RoutesAuthorisedChanged, value: 'routes.authorisedChanged' },
    { id: Events.SiteChanged, value: 'site.changed' },
    { id: Events.UiRefresh, value: 'ui.refresh' }
  ];

  static enumToAcronym(event: Events): string {
    return AcronymsHelper.enumToAcronym(this.events, event);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.events, acronym);
  }
}
