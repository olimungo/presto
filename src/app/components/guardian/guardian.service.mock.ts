import { User } from '../users/user.entity';
import { IConfigurator } from '../configurator/configurator.interface';
import { Configs } from '../configurator/configs.helper';
import { Languages, LanguagesHelper } from '../users/languages.helper';
import { Roles, RolesHelper } from '../users/roles.helper';
import { IGuardian } from './guardian.interface';
import { Events, EventsHelper } from '../events/events.helper';

export class Guardian implements IGuardian {
  private user: User = null;

  /* @ngInject */
  constructor(
    private $rootScope: ng.IRootScopeService, private $q: ng.IQService, private configurator: IConfigurator) {
    this.setListeners();
  }

  login(username: string): void {
    var user: User = null;

    if (username === 'oli') {
      user = new User({
        login: 'mungool',
        rolesAcronym: [ 'ADM', 'REQ' ],
        fullName: 'Olivier Mungo',
        dgUnit: 'OIB.001',
        currentRole: RolesHelper.acronymToEnum('ADM'),
        currentLanguage: LanguagesHelper.acronymToEnum('EN')
      });
    } else if (username === 'pedro') {
      user = new User({
        login: 'pedro',
        fullName: 'Pedro Abelleira Seco',
        rolesAcronym: [ 'REQ', 'PRD' ],
        dgUnit: 'OIB.DR.001',
        currentRole: RolesHelper.acronymToEnum('REQ'),
        currentLanguage: LanguagesHelper.acronymToEnum('FR')
      });
    } else {
      user = new User({
        login: 'voorten',
        fullName: 'Enzo Voort',
        rolesAcronym: [ 'PRD' ],
        dgUnit: 'OIB.OS.2',
        currentRole: RolesHelper.acronymToEnum('PRD'),
        currentLanguage: LanguagesHelper.acronymToEnum('EN')
      });
    }

    this.user = angular.copy(user);
    this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChanged), user);
  }

  logout(): void {
    this.user = null;
    this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChanged));

  }

  isUserAuthenticated(): boolean {
    return this.user !== null;
  }

  isRouteAuthorised(rolesRoute: Roles[]): ng.IPromise<any> {
    var deferred = this.$q.defer();

    if (this.isUserAuthorised(rolesRoute)) {
      deferred.resolve();
    } else {
      deferred.reject();
    }

    return deferred.promise;
  }

  isUserAuthorised(rolesRoute: Roles[]): boolean {
    var result = false;

    if (!this.isUserAuthenticated()) {
      if (rolesRoute.indexOf(Roles.Anonymous) > -1) {
        result = true;
      } else {
        console.log('Unauthorised route');
      }
    } else {
      // Ensure that authenticated user has access to the route
      result = this.isRoleIn(this.user.roles, rolesRoute);
    }

    return result;
  }

  private setListeners() {
    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UserChangedRole), (event: any, currentRole: Roles) => {
      this.user.currentRole = currentRole;
      this.configurator.set(Configs.UserCurrentRole, RolesHelper.enumToAcronym(currentRole));
      this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChanged), angular.copy(this.user));
    });

    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UserChangedLanguage), (event: any, currentLanguage: Languages) => {
      if (this.user) {
        this.user.currentLanguage = currentLanguage;
      }
    });

    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UiRefresh), () => {
      if (this.user) {
        this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChanged), angular.copy(this.user));
      }
    });
  }

  private isRoleIn(rolesUser: Roles[], rolesRoute: Roles[]): boolean {
    return rolesUser.some((role: Roles) => {
      return rolesRoute.indexOf(Roles.All) > -1 || rolesRoute.indexOf(role) > -1;
    });
  }
}
