import { IBackEnd } from '../backEnd/backEnd.interface';
import { User } from '../users/user.entity';
import { IConfigurator } from '../configurator/configurator.interface';
import { Configs } from '../configurator/configs.helper';
import { ITranslator } from '../translations/translator.interface';
import { Languages } from '../users/languages.helper';
import { Roles, RolesHelper } from '../users/roles.helper';
import { IGuardian } from './guardian.interface';
import { Events, EventsHelper } from '../events/events.helper';

export class Guardian implements IGuardian {
  private user: User = null;

  /* @ngInject */
  constructor(
    private $rootScope: ng.IRootScopeService, private $q: ng.IQService, private backEnd: IBackEnd,
    private configurator: IConfigurator, private translator: ITranslator) {
    this.setListeners();
  }

  login(username: string): void {
    this.backEnd.login(username)
      .then((user: User) => {
        user.currentRole = RolesHelper.acronymToEnum(this.configurator.get(Configs.UserCurrentRole, user.rolesAcronym[0]));

        // Check if role in config is still in the user's roles
        // If not, override with the first from the ones just retrieved
        if (user.roles.indexOf(user.currentRole) < 0) {
          user.currentRole = user.roles[0];
          this.configurator.set(Configs.UserCurrentRole, user.rolesAcronym[0]);
        }

        user.currentLanguage = this.translator.currentLanguage;

        this.user = angular.copy(user);
        this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChanged), user);
      })
      .catch((error: any) => {
        // TODO: improve
        console.log('ERROR');
      });
  }

  logout(): void {
    this.backEnd.logout().then((response: any) => {
      this.user = null;
      this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChanged));
    });
  }

  isUserAuthenticated(): boolean {
    return this.user !== null;
  }

  isRouteAuthorised(rolesRoute: Roles[]): ng.IPromise<any> {
    var deferred = this.$q.defer();

    if (this.isUserAuthorised(rolesRoute)) {
      deferred.resolve();
    } else {
      deferred.reject();
    }

    return deferred.promise;
  }

  isUserAuthorised(rolesRoute: Roles[]): boolean {
    var result = false;

    if (!this.isUserAuthenticated()) {
      if (rolesRoute.indexOf(Roles.Anonymous) > -1) {
        result = true;
      } else {
        console.log('Unauthorised route');
      }
    } else {
      // Ensure that authenticated user has access to the route
      result = this.isRoleIn(this.user.roles, rolesRoute);
    }

    return result;
  }

  private setListeners() {
    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UserChangedRole), (event: any, currentRole: Roles) => {
      this.user.currentRole = currentRole;
      this.configurator.set(Configs.UserCurrentRole, RolesHelper.enumToAcronym(currentRole));
      this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChanged), angular.copy(this.user));
    });

    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UserChangedLanguage), (event: any, currentLanguage: Languages) => {
      if (this.user) {
        this.user.currentLanguage = currentLanguage;
      }
    });

    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UiRefresh), () => {
      if (this.user) {
        this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChanged), angular.copy(this.user));
      }
    });
  }

  private isRoleIn(rolesUser: Roles[], rolesRoute: Roles[]): boolean {
    return rolesUser.some((role: Roles) => {
      return rolesRoute.indexOf(Roles.All) > -1 || rolesRoute.indexOf(role) > -1;
    });
  }
}
