import { Roles } from '../users/roles.helper';

export interface IGuardian {
  login(username: string): void;
  logout(): void;
  isUserAuthenticated(): boolean;
  isRouteAuthorised(rolesRoute: Roles[]): ng.IPromise<any>;
  isUserAuthorised(rolesRoute: Roles[]): boolean;
}