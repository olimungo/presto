import { IAcronym, AcronymsHelper } from '../acronyms/acronyms.helper';

export enum Sites { Brussels, Ispra, College, Cie }

export class SitesHelper {
  static sites: IAcronym[] = [
    { id: Sites.Brussels, value: 'BRU' },
    { id: Sites.Ispra, value: 'ISP' },
    { id: Sites.College, value: 'COL' },
    { id: Sites.Cie, value: 'CIE' },
  ];

  static enumToAcronym(site: Sites): string {
    return AcronymsHelper.enumToAcronym(this.sites, site);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.sites, acronym);
  }
}
