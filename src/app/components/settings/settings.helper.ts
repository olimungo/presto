import { IAcronym } from '../core/acronyms/acronyms.interface';
import { AcronymsHelper } from '../core/acronyms/acronyms.helper';

export enum Settings { Sites }

export class SettingsHelper {
  static settings: IAcronym[] = [
    { id: Settings.Sites, value: '/settings/sites' }
  ];

  static enumToAcronym(setting: Settings): string {
    return AcronymsHelper.enumToAcronym(this.settings, setting);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.settings, acronym);
  }
}
