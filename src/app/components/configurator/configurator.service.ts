import { Configs, ConfigsHelper } from './configs.helper';
import { IConfigurator } from './configurator.interface';

export class Configurator implements IConfigurator {
  /* @ngInject */
  constructor(private webStorage: any) {
    webStorage.prefix('config.');
  }

  get(config: Configs, defaultValue?: string): string {
    var result = this.webStorage.get(ConfigsHelper.enumToAcronym(config));

    if (!result && defaultValue) {
      this.set(config, defaultValue);
      result = defaultValue;
    }

    return result;
  }

  set(config: Configs, value: string): void {
    this.webStorage.set(ConfigsHelper.enumToAcronym(config), value);
  }
}
