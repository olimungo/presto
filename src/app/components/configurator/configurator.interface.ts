import { Configs } from './configs.helper';

export interface IConfigurator {
  get(config: Configs, defaultValue?: string): string;
  set(config: Configs, value: string): void;
}
