import { IAcronym, AcronymsHelper } from '../acronyms/acronyms.helper';

export enum Configs { UserLanguage, UserCurrentRole, UserSite }

export class ConfigsHelper {
  static configs: IAcronym[] = [
    { id: Configs.UserLanguage, value: 'user.language' },
    { id: Configs.UserCurrentRole, value: 'user.currentRole' },
    { id: Configs.UserSite, value: 'user.site' }
  ];

  static enumToAcronym(config: Configs): string {
    return AcronymsHelper.enumToAcronym(this.configs, config);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.configs, acronym);
  }
}
