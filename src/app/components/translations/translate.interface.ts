import { ITranslator } from './translator.interface';

export interface ITranslateController {
  translator: ITranslator;
}