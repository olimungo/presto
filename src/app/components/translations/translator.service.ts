import { IConfigurator } from '../configurator/configurator.interface';
import { Configs } from '../configurator/configs.helper';
import { IBackEnd } from '../backEnd/backEnd.interface';
import { Languages, LanguagesHelper } from '../users/languages.helper';
import { TranslateKeys, TranslateKeysHelper } from '../translations/translateKeys.helper';
import { ITranslator } from './translator.interface';

export class Translator implements ITranslator {
  currentLanguage: Languages;
  defaultLanguage: Languages;

  private translations: any = {};

  /* @ngInject */
  constructor(private configurator: IConfigurator, private backEnd: IBackEnd, defaultLanguage: string) {
    this.defaultLanguage = LanguagesHelper.acronymToEnum(defaultLanguage);
  }

  loadTranslations(): ng.IPromise<any> {
    var currentLanguage = this.configurator.get(Configs.UserLanguage, LanguagesHelper.enumToAcronym(this.defaultLanguage));
    this.currentLanguage = LanguagesHelper.acronymToEnum(currentLanguage);

    return this.backEnd.call({ method: 'GET', url: 'languages/' + currentLanguage.toLowerCase() })
      .then((translations: any) => {
        this.translations = translations;
      });
  }

  translate(key: string): string {
    return this.translations[key];
  }

  translateCode(translateKey: TranslateKeys, key: string): string {
    var fullKey: string,
        result = null;

    if (key) {
      fullKey = TranslateKeysHelper.enumToAcronym(translateKey) + key;
      result = this.translate(fullKey);
      result = result ? result : fullKey + ' - MISSING';
    }

    return result;
  }
}
