import { IAcronym, AcronymsHelper } from '../acronyms/acronyms.helper';

export enum TranslateKeys { Roles, Menus, Sites }

export class TranslateKeysHelper {
  static translateKeys: IAcronym[] = [
    { id: TranslateKeys.Roles, value: 'codes.roles.' },
    { id: TranslateKeys.Menus, value: 'codes.menus.' },
    { id: TranslateKeys.Sites, value: 'codes.sites.' }
  ];

  static enumToAcronym(translateKey: TranslateKeys): string {
    return AcronymsHelper.enumToAcronym(this.translateKeys, translateKey);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.translateKeys, acronym);
  }
}
