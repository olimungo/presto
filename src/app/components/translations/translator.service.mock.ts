import { IConfigurator } from '../configurator/configurator.interface';
import { Configs } from '../configurator/configs.helper';
import { Languages, LanguagesHelper } from '../users/languages.helper';
import { TranslateKeys, TranslateKeysHelper } from '../translations/translateKeys.helper';
import { ITranslator } from './translator.interface';

export class Translator implements ITranslator {
  currentLanguage: Languages;
  defaultLanguage: Languages;

  private translations: any = {};

  /* @ngInject */
  constructor(private $q: ng.IQService, private configurator: IConfigurator, defaultLanguage: string) {
    this.defaultLanguage = LanguagesHelper.acronymToEnum(defaultLanguage);
  }

  loadTranslations(): ng.IPromise<any> {
    var deferred = this.$q.defer(),
        currentLanguage = this.configurator.get(Configs.UserLanguage, LanguagesHelper.enumToAcronym(this.defaultLanguage));

    this.currentLanguage = LanguagesHelper.acronymToEnum(currentLanguage);

    if (this.currentLanguage === Languages.English) {
      this.translations = {
        'codes.sites.bru': 'Brussels',
        'codes.sites.isp': 'Ispra',
        'codes.sites.col': 'College',
        'codes.sites.cie': 'CIE'
      };
    } else {
      this.translations = {
        'codes.sites.bru': 'Bruxelles',
        'codes.sites.isp': 'Ispra',
        'codes.sites.col': 'Collège',
        'codes.sites.cie': 'CIE',
        'login.untilEcas': 'Jusqu\'à ce qu\'on implémente ECAS'
      };
    }

    deferred.resolve();

    return deferred.promise;
  }

  translate(key: string): string {
    return this.translations[key];
  }

  translateCode(translateKey: TranslateKeys, key: string): string {
    var fullKey: string,
        result = null;

    if (key) {
      fullKey = TranslateKeysHelper.enumToAcronym(translateKey) + key;
      result = this.translate(fullKey);
      result = result ? result : fullKey + ' - MISSING';
    }

    return result;
  }
}
