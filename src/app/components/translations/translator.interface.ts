import { Languages, LanguagesHelper } from '../users/languages.helper';
import { TranslateKeys, TranslateKeysHelper } from '../translations/translateKeys.helper';

export interface ITranslator {
  currentLanguage: Languages;
  defaultLanguage: Languages;
  loadTranslations(): ng.IPromise<any>;
  translate(key: string): string;
  translateCode(translateKey: TranslateKeys, key: string): string;
}