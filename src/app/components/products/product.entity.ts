export interface IProduct {
  id: string;
  label: string;
}

export class Product implements IProduct {
  id: string;
  label: string;

  constructor(product?: IProduct) {
    if (product) {
      this.id = product.id;
      this.label = product.label;
    }
  }
}
