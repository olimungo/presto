import { IUiLockerController } from './uiLocker.interface';

export function uiLocker(): ng.IDirective {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'app/components/ui/uiLocker/uiLocker.html',
    controller: UiLockerController,
    controllerAs: 'vm',
    bindToController: true
  };
}

class UiLockerController implements IUiLockerController {
  isVisible: boolean = false;

  /* @ngInject */
  constructor(private $rootScope: ng.IRootScopeService) {
    this.setListeners();
  }

  private setListeners(): void {
    this.$rootScope.$on('ui.lock', (event: any, promise: ng.IPromise<any>) => {
      this.isVisible = true;

      promise.then(() => this.isVisible = false);
    });
  }
}
