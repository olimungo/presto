export interface IDropDownController {
  isMenuVisible: boolean;
  items: IDropDownItem[];
  noIcon: boolean;
  itemsElement: any;
  menuElement: any;
  switchVisibility(): void;
  itemSelected(item: IDropDownItem): void;
}

export interface IDropDownItem {
  id: any;
  label: string;
  icon?: string; // Icon in the menu
  borderTop?: boolean; // If a top border has to be drawn in the Settings menu
  originalObject?: any;
}