import { IDropDownController, IDropDownItem  } from './dropDown.interface';

export function dropDown(): ng.IDirective {
  return {
    restrict: 'E',
    bindToController: {
      items: '=',
      onItemSelected: '&',
      noIcon: '&'
    },
    templateUrl: 'app/components/ui/dropDown/dropDown.html',
    controller: DropDownController,
    controllerAs: 'vm',
    scope: {},
    transclude: true,
    link: link
  };
}

class DropDownController implements IDropDownController {
  isMenuVisible: boolean = false;
  items: IDropDownItem[];
  noIcon: boolean = false;
  itemsElement: any;
  menuElement: any;
  onItemSelected: Function;

  private itemsPositioned: boolean = false;

  /* @ngInject */
  constructor (private $document: ng.IDocumentService, private $scope: ng.IScope) {
  }

  switchVisibility(event?: Event): void {
    if (event) {
      event.stopImmediatePropagation();
    }

    if (!this.itemsPositioned) {
      this.itemsPositioned = true;
      var itemsElementLeft = this.itemsElement.offsetLeft - this.itemsElement.offsetWidth + this.menuElement.offsetWidth;
      angular.element(this.itemsElement).css('left', itemsElementLeft + 'px');
    }

    this.isMenuVisible = !this.isMenuVisible;

    if (this.isMenuVisible) {
      this.$document.on('click', () => {
        this.$document.off('click');
        this.switchVisibility();
        this.$scope.$apply();
      });
    } else {
      this.$document.off('click');
    }
  };

  itemSelected(item: IDropDownItem): void {
    this.onItemSelected({ item: item.originalObject });
  }
}

function link(scope: ng.IScope, element: JQuery, attributes: any, vm: IDropDownController) {
  if (attributes.hasOwnProperty('noIcon')) {
    vm.noIcon = true;
  }

  vm.menuElement = element[0];
  vm.itemsElement = element[0].querySelector('.items');
}
