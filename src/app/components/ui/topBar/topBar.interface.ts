import { IDropDownItem } from '../dropDown/dropDown.interface';
import { Languages } from '../../users/languages.helper';
import { IRoute, IAuthorisedRoutes } from '../../routesManager/routesManager.interface';
import { Roles, RolesHelper } from '../../users/roles.helper';

export interface ITopBarController {
  userFullName: string;
  userDgUnit: string;
  userRole: string;
  menuItems : IDropDownItem[];
  settingsMenuItems : IDropDownItem[];
  rolesItems: IDropDownItem[];
  currentLanguage: Languages;
  site: string;
  changeMenu(item: IRoute): void;
  changeRole(item: Roles): void;
}

export interface IMenuItems {
  id: any;
  label: string;
  icon?: string; // Icon in the menu
  originalObject?: any;
}