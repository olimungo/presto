import { IRoute, IAuthorisedRoutes } from '../../routesManager/routesManager.interface';
import { IUser } from '../../users/user.entity';
import { Roles, RolesHelper } from '../../users/roles.helper';
import { TranslateKeys } from '../../translations/translateKeys.helper';
import { Languages } from '../../users/languages.helper';
import { ITranslator } from '../../translations/translator.interface';
import { IDropDownItem } from '../dropDown/dropDown.interface';
import { ITopBarController, IMenuItems } from './topBar.interface';
import { IConfigurator } from '../../configurator/configurator.interface';
import { Configs } from '../../configurator/configs.helper';
import { Events, EventsHelper } from '../../events/events.helper';

export function topBar(): ng.IDirective {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'app/components/ui/topBar/topBar.html',
    controller: TopBarController,
    controllerAs: 'vm',
    bindToController: true
  };
}

class TopBarController implements ITopBarController {
  userFullName: string = null;
  userDgUnit: string = null;
  userRole: string = null;
  menuItems : IMenuItems[] = [];
  settingsMenuItems : IDropDownItem[] = [];
  rolesItems: IDropDownItem[];
  currentLanguage: Languages;
  site: string;

  private userRoles: Roles[];

  /* @ngInject */
  constructor(private $rootScope: ng.IRootScopeService, private $location: ng.ILocationService,
    private translator: ITranslator, configurator: IConfigurator) {
    var site = configurator.get(Configs.UserSite);
    site = site ? site : '';

    this.setListeners();
    this.site = this.translator.translateCode(TranslateKeys.Sites, site.toLowerCase());
    this.currentLanguage = this.translator.currentLanguage;
  }

  changeMenu(route: IRoute): void {
    this.$location.path(route.url);
  }

  changeRole(currentRole: Roles): void {
    this.userRole = this.translator.translateCode(TranslateKeys.Roles, RolesHelper.enumToAcronym(currentRole).toLowerCase());
    this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChangedRole), currentRole);
  }

  private setListeners(): void {
    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.UserChanged), (event: any, user: IUser) => {
      if (user) {
        this.userFullName = user.fullName;
        this.userDgUnit = user.dgUnit;
        this.userRole = this.translator.translateCode(TranslateKeys.Roles, RolesHelper.enumToAcronym(user.currentRole).toLowerCase());
        this.rolesItems = this.createDropDownItemsForRoles(user.roles);
        this.currentLanguage = user.currentLanguage;
      } else {
        this.userFullName = null;
        this.userDgUnit = null;
        this.userRoles = null;
        this.rolesItems = [];
      }
    });

    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.RoutesAuthorisedChanged), (event: any, routes: IAuthorisedRoutes) => {
      this.menuItems = this.createItemsForMenu(routes.generalMenuItems);
      this.settingsMenuItems = this.createDropDownItemsForSettingsMenu(routes.settingsMenuItems);
    });

    this.$rootScope.$on(EventsHelper.enumToAcronym(Events.SiteChanged), (event: any, site: string) => {
      this.site = this.translator.translateCode(TranslateKeys.Sites, site.toLowerCase());
    });
  }

  private createItemsForMenu(routes: IRoute[]): IMenuItems[] {
    return routes.map((r: IRoute) => {
      return {
        id: r.id,
        label: this.translator.translateCode(TranslateKeys.Menus, r.state.toLowerCase()),
        icon: r.icon,
        originalObject: r
      };
    });
  }

  private createDropDownItemsForSettingsMenu(routes: IRoute[]): IDropDownItem[] {
    return routes.map((r: IRoute) => {
      return {
        id: r.id,
        label: this.translator.translateCode(TranslateKeys.Menus, r.state.toLowerCase()),
        icon: r.icon,
        borderTop: r.borderTop,
        originalObject: r
      };
    });
  }

  private createDropDownItemsForRoles(roles: Roles[]): IDropDownItem[] {
    return roles.map((r: Roles) => {
      return {
        id: r,
        label: this.translator.translateCode(TranslateKeys.Roles, RolesHelper.enumToAcronym(r).toLowerCase()),
        originalObject: r
      };
    });
  }
}
