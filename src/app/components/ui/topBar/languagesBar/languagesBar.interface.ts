import { Languages } from '../../../users/languages.helper';

export interface ILanguagesBarController {
  languageEn: Languages;
  languageFr: Languages;
  currentLanguage: Languages;
  setLanguage(language: Languages): void;
}