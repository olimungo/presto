import { Languages, LanguagesHelper } from '../../../users/languages.helper';
import { ILanguagesBarController } from './languagesBar.interface';
import { Events, EventsHelper } from '../../../events/events.helper';
import { Configs } from '../../../../components/configurator/configs.helper';
import { IConfigurator } from '../../../../components/configurator/configurator.interface';

export function languagesBar(): ng.IDirective {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'app/components/ui/topBar/languagesBar/languagesBar.html',
    controller: LanguagesBarController,
    controllerAs: 'vm',
    bindToController: {
      currentLanguage: '='
    }
  };
}

class LanguagesBarController implements ILanguagesBarController {
  languageEn: Languages = Languages.English;
  languageFr: Languages = Languages.French;
  currentLanguage: Languages;

  /* @ngInject */
  constructor(private $rootScope: ng.IRootScopeService, private configurator: IConfigurator) {
  }

  setLanguage(language: Languages): void {
    this.currentLanguage = language;

    this.configurator.set(Configs.UserLanguage, LanguagesHelper.enumToAcronym(language));
    this.$rootScope.$emit(EventsHelper.enumToAcronym(Events.UserChangedLanguage), language);
  }
}
