import { IGuardian } from '../core/guardian/guardian.interface';
import { ILoginFacade } from './login.interface';

export class LoginFacade implements ILoginFacade {
  /* @ngInject */
  constructor(private guardian: IGuardian) {
  }

  login(username: string): void {
    this.guardian.login(username);
  }

  logout(): void {
    this.guardian.logout();
  }
}
