import { ILoginFacade } from './login.interface';

export class LoginController {
  username = 'oli';
  password = 'toto';

  /* @ngInject */
  constructor (private loginFacade: ILoginFacade) {
  }

  login() {
    this.loginFacade.login(this.username);
  }
}
