export interface ILoginFacade {
  login(username: string): void;
}