import { User } from '../users/user.entity';
import { IBackEnd, IBackEndCall } from './backEnd.interface';

export class BackEnd implements IBackEnd {
  /** @ngInject */
  constructor(private $q: ng.IQService) {
  }

  call(params: IBackEndCall): ng.IPromise<any> {
    var deferred = this.$q.defer();

    deferred.resolve(null);

    return deferred.promise;
  }

  login(username: string): ng.IPromise<any> {
    return this.call({ method: 'GET', url: 'login/' + username, responseClass: User })
      .then((user: User) => {
        return user;
      })
      .catch((error: any) => {
        return error;
      });
  }

  logout(): ng.IPromise<any> {
    return this.call({ method: 'GET', url: 'logout' })
      .then((response: any) => {
        return response.data;
      })
      .catch((error: any) => error);
  }
}
