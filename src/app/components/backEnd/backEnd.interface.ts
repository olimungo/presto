export interface IBackEnd {
  call(params: IBackEndCall): ng.IPromise<any>;
  login(username: string): ng.IPromise<any>;
  logout(): ng.IPromise<any>;
}

export interface IBackEndCall {
  method: string;
  url: string;
  queryParams?: string[];
  data?: any;
  responseClass?: any;
}