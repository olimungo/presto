export enum Roles {
  Anonymous,
  All,
  Administrator,
  Requestor,
  OrderAdministrator,
  CounterSigning,
  PrestataireDrinks,
  PrestataireBanqueting,
  PrestatairePrivate,
  PrestataireConference
};

export interface IRolesHelpers {
  acronymToEnum(acronym: string): Roles;
}

interface IRolesAcronym {
  id: Roles;
  value: string;
}

export class RolesHelper implements IRolesHelpers {
  private rolesAcronym: IRolesAcronym[] = [
    { id: Roles.Anonymous, value: 'ANO' },
    { id: Roles.All, value: 'ALL' },
    { id: Roles.Administrator, value: 'ADM' },
    { id: Roles.Requestor, value: 'REQ' },
    { id: Roles.OrderAdministrator, value: 'ORA' },
    { id: Roles.CounterSigning, value: 'COS' },
    { id: Roles.PrestataireDrinks, value: 'PRD' },
    { id: Roles.PrestataireBanqueting, value: 'PRB' },
    { id: Roles.PrestatairePrivate, value: 'PRP' },
    { id: Roles.PrestataireConference, value: 'PRC' }
  ];

  acronymToEnum(acronym: string): Roles {
    var result = this.rolesAcronym.filter((r: IRolesAcronym) => r.value === acronym);

    return result.length > 0 ? result[0].id : null;
  }

  enumToAcronym(id: Roles): string {
    var result = this.rolesAcronym.filter((r: IRolesAcronym) => r.id === id);

    return result.length > 0 ? result[0].value : null;
  }
}

