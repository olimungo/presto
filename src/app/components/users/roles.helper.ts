import { IAcronym, AcronymsHelper } from '../acronyms/acronyms.helper';

export enum Roles {
  Anonymous,
  All,
  Administrator,
  Requestor,
  OrderAdministrator,
  CounterSigning,
  PrestataireDrinks,
  PrestataireBanqueting,
  PrestatairePrivate,
  PrestataireConference
};

export class RolesHelper {
  static roles: IAcronym[] = [
    { id: Roles.Anonymous, value: 'ANO' },
    { id: Roles.All, value: 'ALL' },
    { id: Roles.Administrator, value: 'ADM' },
    { id: Roles.Requestor, value: 'REQ' },
    { id: Roles.OrderAdministrator, value: 'ORA' },
    { id: Roles.CounterSigning, value: 'COS' },
    { id: Roles.PrestataireDrinks, value: 'PRD' },
    { id: Roles.PrestataireBanqueting, value: 'PRB' },
    { id: Roles.PrestatairePrivate, value: 'PRP' },
    { id: Roles.PrestataireConference, value: 'PRC' }
  ];

  static enumToAcronym(role: Roles): string {
    return AcronymsHelper.enumToAcronym(this.roles, role);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.roles, acronym);
  }
}
