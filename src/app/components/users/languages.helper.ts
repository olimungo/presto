import { IAcronym, AcronymsHelper } from '../acronyms/acronyms.helper';

export enum Languages { English, French }

export class LanguagesHelper {
  static languages: IAcronym[] = [
    { id: Languages.English, value: 'EN' },
    { id: Languages.French, value: 'FR' }
  ];

  static enumToAcronym(language: Languages): string {
    return AcronymsHelper.enumToAcronym(this.languages, language);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.languages, acronym);
  }
}
