import { Roles, RolesHelper } from './roles.helper';
import { Languages } from './languages.helper';

export interface IUser {
  login: string;
  rolesAcronym: string[];
  roles: Roles[];
  fullName: string;
  dgUnit: string;
  currentRole: Roles;
  currentLanguage: Languages;
}

export class User implements IUser {
  login: string;
  rolesAcronym: string[];
  roles: Roles[];
  fullName: string;
  dgUnit: string;
  currentRole: Roles;
  currentLanguage: Languages;

  constructor(user?: any) {
    if (user) {
      this.login = user.login;
      this.rolesAcronym = user.rolesAcronym;
      this.roles = user.rolesAcronym.map((r: string) => RolesHelper.acronymToEnum(r));
      this.fullName = user.fullName;
      this.dgUnit = user.dgUnit;
      this.currentRole = user.currentRole;
      this.currentLanguage = user.currentLanguage;
    }
  }
}
