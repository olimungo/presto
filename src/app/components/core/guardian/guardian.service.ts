import { IBackEnd } from '../backEnd/backEnd.interface';
import { IUser } from '../users/user.interface';
import { ERoles } from '../users/roles.helper';
import { IGuardian } from './guardian.interface';

export class Guardian implements IGuardian {
  private user: IUser = null;

  /* @ngInject */
  constructor(
    private $rootScope: ng.IRootScopeService, private $q: ng.IQService, private backEnd: IBackEnd) {
  }

  login(username: string): ng.IPromise<any> {
    return this.backEnd.login(username)
      .then((user: IUser) => {
        this.user = user;

        return this.user;
      })
      .catch((error: any) => {
        // TODO: improve
        console.log('ERROR');
      });
  }

  logout(): ng.IPromise<any> {
    return this.backEnd.logout().then((response: any) => {
      this.user = null;
    });
  }

  changeRole(role: ERoles): void {
    this.user.role = role;
  }

  isUserAuthenticated(): boolean {
    return this.user !== null;
  }

  isRouteAuthorised(roles: ERoles[]): ng.IPromise<any> {
    var deferred = this.$q.defer();

    if (this.isUserAuthorised(roles)) {
      deferred.resolve();
    } else {
      deferred.reject();
    }

    return deferred.promise;
  }

  isUserAuthorised(roles: ERoles[]): boolean {
    var result = false;

    if (!this.isUserAuthenticated()) {
      if (roles.indexOf(ERoles.Anonymous) > -1) {
        result = true;
      }
    } else {
      // Ensure that authenticated user has access to the route
      result = this.isRoleIn(this.user.role, roles);
    }

    if (!result) {
      console.log('Unauthorised route');
    }

    return result;
  }

  isRoleIn(roleUser: ERoles, rolesRoute: ERoles[]): boolean {
    rolesRoute = rolesRoute || [];

    return rolesRoute.some((role: ERoles) => ((role === ERoles.All && role !== ERoles.Anonymous) || role === roleUser));
  }
}
