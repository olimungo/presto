import { ERoles } from '../users/roles.helper';

// Takes care of authentications and authorisations
// Keeps track of the connected user
export interface IGuardian {
  // Login a user
  // TODO: should redirect to ECAS
  login(username: string): ng.IPromise<any>;

  // Log out a user
  logout(): ng.IPromise<any>;

  // Change the role of the current user
  changeRole(role: ERoles): void;

  // Check if user is currently logged in
  isUserAuthenticated(): boolean;

  // Check if route is authorised
  isRouteAuthorised(roles: ERoles[]): ng.IPromise<any>;

  // Check if user is authorised
  isUserAuthorised(roles: ERoles[]): boolean;

  // Check if role is included in an array of roles
  isRoleIn(role: ERoles, rolesRoute: ERoles[]): boolean;
}