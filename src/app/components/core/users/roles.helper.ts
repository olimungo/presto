import { IAcronym } from '../acronyms/acronyms.interface';
import { AcronymsHelper } from '../acronyms/acronyms.helper';

export enum ERoles {
  Anonymous,
  All,
  Administrator,
  Requestor,
  OrderAdministrator,
  CounterSigning,
  PrestataireDrinks,
  PrestataireBanqueting,
  PrestatairePrivate,
  PrestataireConference
};

export class ERolesHelper {
  static roles: IAcronym[] = [
    { id: ERoles.Anonymous, value: 'ANO' },
    { id: ERoles.All, value: 'ALL' },
    { id: ERoles.Administrator, value: 'ADM' },
    { id: ERoles.Requestor, value: 'REQ' },
    { id: ERoles.OrderAdministrator, value: 'ORA' },
    { id: ERoles.CounterSigning, value: 'COS' },
    { id: ERoles.PrestataireDrinks, value: 'PRD' },
    { id: ERoles.PrestataireBanqueting, value: 'PRB' },
    { id: ERoles.PrestatairePrivate, value: 'PRP' },
    { id: ERoles.PrestataireConference, value: 'PRC' }
  ];

  static enumToAcronym(role: ERoles): string {
    return AcronymsHelper.enumToAcronym(this.roles, role);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.roles, acronym);
  }
}
