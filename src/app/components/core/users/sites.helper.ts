import { IAcronym } from '../acronyms/acronyms.interface';
import { AcronymsHelper } from '../acronyms/acronyms.helper';

export enum ESites { Brussels, Ispra, College, Cie }

export class ESitesHelper {
  static sites: IAcronym[] = [
    { id: ESites.Brussels, value: 'BRU' },
    { id: ESites.Ispra, value: 'ISP' },
    { id: ESites.College, value: 'COL' },
    { id: ESites.Cie, value: 'CIE' },
  ];

  static enumToAcronym(site: ESites): string {
    return AcronymsHelper.enumToAcronym(this.sites, site);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.sites, acronym);
  }
}
