import { ERoles } from './roles.helper';
import { ELanguages } from '../translations/languages.helper';
import { ESites } from './sites.helper';

// Represent a user in the system
export interface IUser {
  // User's login (username)
  login: string;

  // Array of roles by their acronym
  rolesAcronym: string[];

  // Array of roles by their id
  roles: ERoles[];

  // List of translated roles labels
  rolesList: string;

  // User's full name
  fullName: string;

  // User's DG and unit
  dgUnit: string;

  // User's current role
  role: ERoles;

  // User's current site
  site: ESites;
}