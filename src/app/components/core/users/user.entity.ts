import { ERoles, ERolesHelper } from './roles.helper';
import { ESites } from './sites.helper';
import { IUser } from './user.interface';

export class User implements IUser {
  login: string;
  rolesAcronym: string[];
  roles: ERoles[];
  rolesList: string;
  fullName: string;
  dgUnit: string;
  role: ERoles;
  site: ESites;

  constructor(user?: any) {
    if (user) {
      this.login = user.login;
      this.rolesAcronym = user.rolesAcronym;
      this.roles = user.rolesAcronym.map((role: string) => ERolesHelper.acronymToEnum(role));
      this.rolesList = user.rolesList;
      this.fullName = user.fullName;
      this.dgUnit = user.dgUnit;
      this.role = user.role;
      this.site = user.site;
    }
  }
}
