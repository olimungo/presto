import { IUser } from '../users/user.interface';
import { User } from '../users/user.entity';
import { IBackEnd, IBackEndCall } from './backEnd.interface';

export class BackEnd implements IBackEnd {
  /** @ngInject */
  constructor(private $http: ng.IHttpService, private backEndUrl: string, private useDataMockUps: boolean) {
  }

  call(params: IBackEndCall): ng.IPromise<any> {
    var url = this.backEndUrl + params.url;

    if (this.useDataMockUps) {
      url = url + '.json';
    }

    return this.$http({ method: params.method, url: url, data: params.data })
      .then((response: any) => {
        return this.transformToResponseClass(response.data, params.responseClass);
      })
      .catch((error: any) => error);
  }

  login(username: string): ng.IPromise<any> {
    return this.call({ method: 'GET', url: 'login/' + username, responseClass: User })
      .then((user: IUser) => {
        // Receiving an answer means a valid authentication.
        // If authentication failed on the server, it should return a
        // "401 unauthorized access" error which should be handled in the
        // "catch" block.
        // TODO: although if there's an error, we may get here, so improve this part
        return user;
      })
      .catch((error: any) => {
        return error;
      });
  }

  logout(): ng.IPromise<any> {
    return this.call({ method: 'GET', url: 'logout' })
      .then((response: any) => {
        return response.data;
      })
      .catch((error: any) => error);
  }

  private transformToResponseClass(data: any, responseClass: any): any {
    if (responseClass) {
      if (Array.isArray(data)) {
        return data.map((element: any) => new responseClass(element));
      } else {
        return new responseClass(data);
      }
    } else {
      return data;
    }
  }
}
