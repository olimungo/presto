// Wrapper around $http service
// Keeps track of the token returned by an ECAS authentication
export interface IBackEnd {
  // REST call to the back-end server
  call(params: IBackEndCall): ng.IPromise<any>;

  // Authenticate a user and keep the returned token
  login(username: string): ng.IPromise<any>;

  // Clear the user's token
  logout(): ng.IPromise<any>;
}

export interface IBackEndCall {
  // GET, POST, PUT, DELETE
  method: string;

  // REST resource
  url: string;

  // Data to send via the request's body
  data?: any;

  // Class to which the received data structure will be converted
  // If an array is retrieved, each row will be converted
  // If no class is specified, data structure is returned as is
  responseClass?: any;
}