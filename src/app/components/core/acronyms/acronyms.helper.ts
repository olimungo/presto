import { IAcronym } from './acronyms.interface';

export class AcronymsHelper {
  static enumToAcronym(acronyms: IAcronym[], id: any): string {
    var result = acronyms.filter((i: IAcronym) => i.id === id);

    return result.length > 0 ? result[0].value : null;
  }

  static acronymToEnum(acronyms: IAcronym[], acronym: string): any {
    var result = acronyms.filter((i: IAcronym) => i.value === acronym);

    return result.length > 0 ? result[0].id : null;
  }
}
