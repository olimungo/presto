// Helps transforming enumarations to acronyms and vice-versa
export interface IAcronym {
  // Id of the acronym
  id: any;

  // Value of the acronym
  value: string;
}