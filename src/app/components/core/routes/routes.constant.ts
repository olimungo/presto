import { ERoles } from '../users/roles.helper';
import { ERoutes, IRoute, EMenuTypes } from './routes.interface';

// Holds the routes describing the front-end routing
// This class is defined as a Angular constant in index.module.ts
export class Routes {
  static routes: IRoute[] = [
    {
      id: ERoutes.Dashboards, url: '/dashboards',  state: 'dashboards', template: 'app/dashboards/dashboards.html',
      controller: 'DashboardsController', roles: [ ERoles.Requestor, ERoles.OrderAdministrator, ERoles.Administrator ],
      menuType: EMenuTypes.General, order: 1, icon: 'fa-bar-chart',
      defaultForRoles : [ ERoles.OrderAdministrator ]
    },
    {
      id: ERoutes.Login, url: '/login',  state: 'login', template: 'app/connection/login.html',
      controller: 'ConnectionController', roles: [ ERoles.Anonymous ], defaultRoute: true
    },
    {
      id: ERoutes.Logout, url: '/logout',  state: 'logout', template: 'app/connection/logout.html',
      controller: 'ConnectionController', roles: [ ERoles.All ], menuType: EMenuTypes.Settings, order: 4,
      icon: 'fa-sign-out', borderTop: true
    },
    {
      id: ERoutes.Orders, url: '/orders',  state: 'orders', template: 'app/orders/orders.html',
      controller: 'OrdersController',
      roles: [
        ERoles.Requestor, ERoles.CounterSigning, ERoles.PrestataireDrinks, ERoles.PrestataireBanqueting, ERoles.PrestataireConference,
        ERoles.PrestatairePrivate, ERoles.Administrator
      ],
      menuType: EMenuTypes.General, order: 2, icon: 'fa-file-text-o',
      defaultForRoles : [ ERoles.Requestor ]
    },
    {
      id: ERoutes.Catalog, url: '/products',  state: 'products', template: 'app/products/products.html',
      controller: 'ProductsController', roles: [ ERoles.Administrator ], menuType: EMenuTypes.Settings, order: 1, icon: 'fa-sitemap'
    },
    {
      id: ERoutes.Reports, url: '/reports',  state: 'reports', template: 'app/reports/reports.html',
      controller: 'ReportsController',
      roles: [
        ERoles.Requestor, ERoles.OrderAdministrator, ERoles.CounterSigning, ERoles.PrestataireDrinks, ERoles.PrestataireBanqueting,
        ERoles.PrestataireConference, ERoles.PrestatairePrivate, ERoles.Administrator
      ],
      menuType: EMenuTypes.General, order: 3, icon: 'fa-line-chart',
      defaultForRoles : [ ERoles.CounterSigning ]
    },
    {
      id: ERoutes.Settings, url: '/settings',  state: 'settings', template: 'app/settings/settings.html',
      controller: 'SettingsController', roles: [ ERoles.Administrator ], menuType: EMenuTypes.Settings, order: 3, icon: 'fa-cog',
      defaultForRoles : [ ERoles.Administrator ]
    },
    {
      id: ERoutes.Settings, url: '/settings/sites',  state: 'settings/sites', template: 'app/settings/sites/sites.html',
      controller: 'SitesController', roles: [ ERoles.Administrator ]
    },
    {
      id: ERoutes.Users, url: '/users',  state: 'users', template: 'app/users/users.html',
      controller: 'UsersController', roles: [ ERoles.Administrator ], menuType: EMenuTypes.Settings, order: 2, icon: 'fa-users',
      defaultForRoles : [ ]
    }
  ];

  static get(): IRoute[] {
    return this.routes;
  }
}
