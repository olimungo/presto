import { ERoles } from '../users/roles.helper';
import { IUser } from '../users/user.interface';
import { ERoutes } from './routes.interface';
import { IRoutesManager, IAuthorisedRoutes, IRoute } from './routesManager.interface';
import { EMenuTypes } from './routes.interface';
import { EEvents, EEventsHelper } from '../../shell/events/events.helper';

export class RoutesManager implements IRoutesManager {
  private $rootScope;
  private $location;

  /* @ngInject */
  $get($rootScope: ng.IRootScopeService, $location: ng.ILocationService): void {
    this.$rootScope = $rootScope;
    this.$location = $location;
    // this.setListeners();
  }

/*
  private setListeners(): void {
    this.$rootScope.$on(EEventsHelper.enumToAcronym(EEvents.UserChanged), (event: any, user: IUser) => {
      if (user) {
        this.setAllowedRoutes(user.role);
      } else {
        this.setAllowedRoutes(ERoles.Anonymous);
      }
    });
  }

  private setAllowedRoutes(role?: ERoles) {
    var filteredRoutes = this.filterRoutes(role);

    this.$rootScope.$emit(EEventsHelper.enumToAcronym(EEvents.RoutesAuthorisedChanged), filteredRoutes);

    if (filteredRoutes.defaultRoute !== null) {
      this.$location.path(filteredRoutes.defaultRoute.url);
    }
  }

  private filterRoutes(role: Roles): IAuthorisedRoutes {
    var filteredRoutesByRoles = this._routes
      .filter((route: IRoute) => this.isRoleIn(role, route.roles));

    var defaultRoutes = filteredRoutesByERoles.filter((route: IRoute) => {
      return this.isRoleIn(role, route.defaultForRoles);
    });

    var defaultRoute = null;

    if (defaultRoutes.length > 0) {
      defaultRoute = defaultRoutes[0];
    }

    var generalMenuItems: IRoute[] = filteredRoutesByRoles
      .filter((route: IRoute) => route.menuType === EMenuTypes.General)
      .sort(this.sortRoutes);

    var settingsMenuItems: IRoute[] = filteredRoutesByRoles
      .filter((route: IRoute) => route.menuType === EMenuTypes.Settings)
      .sort(this.sortRoutes);

    return {
      generalMenuItems: generalMenuItems,
      settingsMenuItems: settingsMenuItems,
      defaultRoute: defaultRoute
    };
  }

  private sortRoutes(route1: IRoute, route2: IRoute) {
    return route1.order - route2.order;
  }

  private isRoleIn(role: ERoles, rolesRoute: ERoles[]): boolean {
    rolesRoute = rolesRoute || [];

    return rolesRoute.some((role: ERoles) => ((role === ERoles.All && role !== ERoles.Anonymous) || role === role));
  }

  private getRouteById(id: ERoutes) {
    var result = this._routes.filter((route: IRoute) => route.id === id);

    return result.length > 0 ? result[0] : null;
  }
*/
}
