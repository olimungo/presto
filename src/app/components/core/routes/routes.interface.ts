import { ERoles } from '../users/roles.helper';

export enum ERoutes {
  Catalog, Dashboards, Login, Logout, Orders, Reports,
  Users, Settings, Sites
}

export enum EMenuTypes { General, Settings }

export interface IRoute {
  id: ERoutes;

  // URL of the route
  url: string;

  // State of the route
  state: string;

  // Template's URL (view)
  template: string;

  // Controller for this route when instantiating the corresponding view
  controller: string;

  // Roles having access to this route
  roles: ERoles[];

  // In which menu the route has to be displayed (General or Settings)
  menuType?: EMenuTypes;

  // The order in which the route has to be displayed
  order?: number;

  // Icon in the menu
  icon?: string;

  // If a top border has to be drawn in the Settings menu
  borderTop?: boolean;

  // Roles for which this route is considered as default (after being logged in)
  defaultForRoles?: ERoles[];

  // Is this the default route
  defaultRoute?: boolean;
}
