import { ERoutes } from './routes.interface';
import { ERoles } from '../users/roles.helper';
import { EMenuTypes } from './routes.interface';

export interface IRoutesManager {
}

export interface IAuthorisedRoutes {
  generalMenuItems: IRoute[];
  settingsMenuItems: IRoute[];
  defaultRoute: IRoute;
}

export interface IRoute {
  id: ERoutes;
  label: string;
  url: string;
  state: string;
  template: string;
  controller: string;
  roles: ERoles[];
  menuType?: EMenuTypes; // In which menu the route has to be displayed
  order?: number; // The order in which the route has to be displayed
  icon?: string; // Icon in the menu
  borderTop?: boolean; // If a top border has to be drawn in the Settings menu
  defaultForRoles?: ERoles[];
}