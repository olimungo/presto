import { IAcronym } from '../acronyms/acronyms.interface';
import { AcronymsHelper } from '../acronyms/acronyms.helper';

export enum ETranslateKeys { Roles, Menus, Sites }

export class ETranslateKeysHelper {
  static translateKeys: IAcronym[] = [
    { id: ETranslateKeys.Roles, value: 'codes.roles.' },
    { id: ETranslateKeys.Menus, value: 'codes.menus.' },
    { id: ETranslateKeys.Sites, value: 'codes.sites.' }
  ];

  static enumToAcronym(translateKey: ETranslateKeys): string {
    return AcronymsHelper.enumToAcronym(this.translateKeys, translateKey);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.translateKeys, acronym);
  }
}
