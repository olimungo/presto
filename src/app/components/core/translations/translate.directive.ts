import { ITranslator } from './translator.interface';
import { ITranslateController } from './translate.interface';

export function translate(): ng.IDirective {
  return {
    restrict: 'A',
    templateUrl: 'app/components/core/translations/translate.html',
    controller: TranslateController,
    controllerAs: 'vm',
    link: link,
    transclude: true,
    scope: {}
  };
}

class TranslateController implements ITranslateController {
  /* @ngInject */
  constructor(public translator: ITranslator) {
  }
}

function link(scope: ng.IScope, element: JQuery, attributes: any, vm: ITranslateController) {
    var text: string;
    var values = attributes.values ? attributes.values.split(',') : [];
    var isTranslated = true;

    if (vm.translator.currentLanguage !== vm.translator.defaultLanguage) {
      text = vm.translator.translate(attributes.translate);

      if (!text) {
        text = element[0].innerText;
        isTranslated = false;
      }
    } else {
      text = element[0].innerText;
    }

    values.forEach((value: string) => text = text.replace('##', value));

    element[0].innerText = text;

    if (!isTranslated) {
      element.addClass('translate-directive missing-translation');
    }
}
