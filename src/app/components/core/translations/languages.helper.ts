import { IAcronym } from '../acronyms/acronyms.interface';
import { AcronymsHelper } from '../acronyms/acronyms.helper';

export enum ELanguages { English, French }

export class ELanguagesHelper {
  static languages: IAcronym[] = [
    { id: ELanguages.English, value: 'EN' },
    { id: ELanguages.French, value: 'FR' }
  ];

  static enumToAcronym(language: ELanguages): string {
    return AcronymsHelper.enumToAcronym(this.languages, language);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.languages, acronym);
  }
}
