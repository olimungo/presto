import { IConfigurator } from '../configurator/configurator.interface';
import { EConfigs } from '../configurator/configs.helper';
import { IBackEnd } from '../backEnd/backEnd.interface';
import { ELanguages, ELanguagesHelper } from '../translations/languages.helper';
import { ETranslateKeys, ETranslateKeysHelper } from '../translations/translateKeys.helper';
import { ITranslator } from './translator.interface';

export class Translator implements ITranslator {
  currentLanguage: ELanguages;
  defaultLanguage: ELanguages;

  private translations: any = {};

  /* @ngInject */
  constructor(private configurator: IConfigurator, private backEnd: IBackEnd, defaultLanguage: string) {
    this.defaultLanguage = ELanguagesHelper.acronymToEnum(defaultLanguage);
  }

  loadTranslations(): ng.IPromise<any> {
    var currentLanguage = this.configurator.get(EConfigs.UserLanguage, ELanguagesHelper.enumToAcronym(this.defaultLanguage));
    this.currentLanguage = ELanguagesHelper.acronymToEnum(currentLanguage);

    return this.backEnd.call({ method: 'GET', url: 'languages/' + currentLanguage.toLowerCase() })
      .then((translations: any) => {
        this.translations = translations;
      });
  }

  translate(key: string): string {
    return this.translations[key];
  }

  translateCode(translateKey: ETranslateKeys, key: string): string {
    var fullKey: string,
        result = null;

    if (key) {
      fullKey = ETranslateKeysHelper.enumToAcronym(translateKey) + key;
      result = this.translate(fullKey);
      result = result ? result : fullKey + ' - MISSING';
    }

    return result;
  }
}
