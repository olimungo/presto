import { ELanguages, ELanguagesHelper } from '../translations/languages.helper';
import { ETranslateKeys, ETranslateKeysHelper } from './translateKeys.helper';

// Takes care of loading translations and serving translations requests
export interface ITranslator {
  // Current language of the UI
  currentLanguage: ELanguages;

  // Default language of the application
  defaultLanguage: ELanguages;

  // Load translations asynchronously
  loadTranslations(): ng.IPromise<any>;

  // Regular translate request
  // Usually used through the directive "translate"
  translate(key: string): string;

  // Code translate request (like roles, sites or environments)
  // Usually used in controllers or facades in order to get a translation of
  // acronyms
  translateCode(translateKey: ETranslateKeys, key: string): string;
}