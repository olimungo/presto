import { EConfigs, EConfigsHelper } from './configs.helper';
import { IConfigurator } from './configurator.interface';

export class Configurator implements IConfigurator {
  /* @ngInject */
  constructor(private webStorage: any) {
    webStorage.prefix('config.');
  }

  get(config: EConfigs, defaultValue?: string): string {
    var result = this.webStorage.get(EConfigsHelper.enumToAcronym(config));

    if (!result && defaultValue) {
      this.set(config, defaultValue);
      result = defaultValue;
    }

    return result;
  }

  set(config: EConfigs, value: string): void {
    this.webStorage.set(EConfigsHelper.enumToAcronym(config), value);
  }
}
