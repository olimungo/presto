import { IAcronym } from '../acronyms/acronyms.interface';
import { AcronymsHelper } from '../acronyms/acronyms.helper';

export enum EConfigs { UserLanguage, UserCurrentRole, UserSite }

export class EConfigsHelper {
  static configs: IAcronym[] = [
    { id: EConfigs.UserLanguage, value: 'user.language' },
    { id: EConfigs.UserCurrentRole, value: 'user.currentRole' },
    { id: EConfigs.UserSite, value: 'user.site' }
  ];

  static enumToAcronym(config: EConfigs): string {
    return AcronymsHelper.enumToAcronym(this.configs, config);
  }

  static acronymToEnum(acronym: string): any {
    return AcronymsHelper.acronymToEnum(this.configs, acronym);
  }
}
