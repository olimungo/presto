import { EConfigs } from './configs.helper';

// Takes care of reading and writing key/value pairs in the local storage
export interface IConfigurator {
  // Get a key from local storage. If key is not found and a default value
  // is set in the params, then create that key
  get(config: EConfigs, defaultValue?: string): string;

  // Write a key in the local storage
  set(config: EConfigs, value: string): void;
}
