import { IUsersFacade } from './users.interface';
import { ITranslator } from '../components/core/translations/translator.interface';
import { IUser } from '../components/core/users/user.interface';
import { User } from '../components/core/users/user.entity';
import { IBackEnd } from '../components/core/backEnd/backEnd.interface';
import { ETranslateKeys } from '../components/core/translations/translateKeys.helper';
import { ERolesHelper } from '../components/core/users/roles.helper';

export class UsersFacade implements IUsersFacade {
  /* @ngInject */
  constructor(private backEnd: IBackEnd, private translator: ITranslator) {
  }

  getUsers(): ng.IPromise<any> {
    return this.backEnd.call({method: 'GET', url: 'users', responseClass: User}).then((users: IUser[]) => {
      return users.map((user: IUser) => {
        user.rolesList = this.getRolesList(user);

        return user;
      });
    });
  }

  private getRolesList(user: IUser): string {
    return user.roles.map((role: any) => this.translator.translateCode(ETranslateKeys.Roles, ERolesHelper.enumToAcronym(role).toLowerCase()))
      .join(', ');
  }
}
