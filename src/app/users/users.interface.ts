// Takes care of all the dirty work related to Users
export interface IUsersFacade {
  // Get the list of all users
  getUsers(): ng.IPromise<any>;
}