import { IUser } from '../components/core/users/user.interface';

export function usersFilter() {
  return function(users: IUser[], pattern: string) {
    var filtered: IUser[] = users;

    if (pattern) {
      filtered = users.filter((user: IUser) => {
        var expr = (
          user.login + ' '
          + user.fullName + ' '
          + user.dgUnit + ' '
          + user.rolesList
        ).toLowerCase();

        return expr.indexOf(pattern.toLowerCase()) > -1;
      });
    }

    return filtered;
  };
}
