import { IUsersFacade } from './users.interface';
import { ITranslator } from '../components/core/translations/translator.interface';
import { IUser } from '../components/core/users/user.interface';
import { User } from '../components/core/users/user.entity';
import { ETranslateKeys } from '../components/core/translations/translateKeys.helper';
import { ERolesHelper } from '../components/core/users/roles.helper';

declare var faker: any;

export class UsersFacade implements IUsersFacade {
  /* @ngInject */
  constructor(private $q: ng.IQService, private translator: ITranslator) {
  }

  getUsers(): ng.IPromise<any> {
    var QUANTITY = 10,
        users: IUser[] = [],
        deferred = this.$q.defer();

    for (var i = 0; i < QUANTITY; i++) {
      users.push(this.createUser());
    }

    users = users.map((user: IUser) => {
      user.rolesList = this.getRolesList(user);
      return user;
    });

    deferred.resolve(users);

    return deferred.promise;
  }

  private createUser(): IUser {
    return new User({
      login: faker.internet.userName().toLowerCase(),
      rolesAcronym: [ 'REQ' ],
      fullName: faker.name.findName(),
      dgUnit: 'OIB.1'
    });
  }

  private getRolesList(user: IUser): string {
    return user.roles.map((role: any) => this.translator.translateCode(ETranslateKeys.Roles, ERolesHelper.enumToAcronym(role).toLowerCase()))
      .join(', ');
  }
}
