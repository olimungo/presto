import { IUsersFacade } from './users.interface';
import { IUser } from '../components/core/users/user.interface';

export class UsersController {
  users: IUser[];

  /* @ngInject */
  constructor (private usersFacade: IUsersFacade) {
    this.usersFacade.getUsers().then((users: IUser[]) => {
      this.users = users;
    });
  }
}
