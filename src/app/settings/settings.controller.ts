import { Settings, SettingsHelper } from '../components/settings/settings.helper';

export class SettingsController {
  settingSites: Settings = Settings.Sites;

  /* @ngInject */
  constructor (private $location: ng.ILocationService) {
    //
  }

  settingSelected(setting: Settings): void {
    this.$location.path(SettingsHelper.enumToAcronym(setting));
  }
}
