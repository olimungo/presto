import { ESites } from '../../components/core/users/sites.helper';
import { EEvents, EEventsHelper } from '../../components/shell/events/events.helper';

// TODO: add a service as a Facade to emit an event
export class SitesController {
  siteBrussels: ESites = ESites.Brussels;
  siteIspra: ESites = ESites.Ispra;
  siteCollege: ESites = ESites.College;
  siteCie: ESites = ESites.Cie;

  /* @ngInject */
  constructor (private $rootScope: ng.IRootScopeService) {
  }

  changeSite(site: ESites): void {
    this.$rootScope.$emit(EEventsHelper.enumToAcronym(EEvents.SiteChanged), site);
  }
}
