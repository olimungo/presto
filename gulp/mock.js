/* global require */
'use strict';

var gulp = require('gulp');
var replace = require('gulp-replace');

gulp.task('mock', function(){
  gulp.src('src/app/index.module.ts')
    .pipe(replace(/.(service|directive)(';\s*\/\* (mock) \*\/)/g, '.$1.mock$2'))
    .pipe(gulp.dest('.', { cwd: 'src/app/'}));
});

gulp.task('unmock', function(){
  gulp.src('src/app/index.module.ts')
    .pipe(replace(/.(service|directive).mock(';\s*\/\* (mock) \*\/)/g, '.$1$2'))
    .pipe(gulp.dest('.', { cwd: 'src/app/'}));
});