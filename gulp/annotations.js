/* global require */
'use strict';

var gulp = require('gulp');
var replace = require('gulp-replace');

gulp.task('mock', function(){
  gulp.src('src/app/index.module.ts')
    .pipe(replace(/.(service|directive|facade)(';\s*\/\* (mock) \*\/)/g, '.$1.mock$2'))
    .pipe(gulp.dest('.', { cwd: 'src/app/'}));
});

gulp.task('unmock', function(){
  gulp.src('src/app/index.module.ts')
    .pipe(replace(/.(service|directive|facade).mock(';\s*\/\* (mock) \*\/)/g, '.$1$2'))
    .pipe(gulp.dest('.', { cwd: 'src/app/'}));
});

gulp.task('env:dev', function() {
  setEnvironment('DEV');
});

gulp.task('env:test', function() {
  setEnvironment('TEST');
});

gulp.task('env:acc', function() {
  setEnvironment('ACC');
});

gulp.task('env:prod', function() {
  setEnvironment('');
});

function setEnvironment(env) {
  gulp.src('src/app/index.module.ts')
    .pipe(replace(/(.constant\('environment',\s*')(\w*)('\)\s*\/\*\s*env\s*\*\/)/g, '$1' + env + '$3'))
    .pipe(gulp.dest('.', { cwd: 'src/app/'}));
}